#!/usr/bin/env python3

import cchardet
import pygments.lexers.c_cpp
import pygments.lexers.python
from pygments.token import Token
import sys

buf_size = 128
cur_stdin_buf = b''

# This may be a bit of overkill in the name of performance, but reading the
# entire stdin and then going through it again to parse and tokenize it feels
# like underkill. None of this would be necessary if python had a sensible way
# of reading stdin by line with an arbitrary encoding, but apparently it doesn't.
def read_stdin_until(stop: bytes) -> bytes:
    global buf_size, cur_stdin_buf
    stop_len = len(stop)
    ret_list = []
    def get_ret(extras):
        return b''.join(ret_list + extras)
    stop_ind = cur_stdin_buf.find(stop)
    while stop_ind < 0:
        more_buf = sys.stdin.buffer.read(buf_size)
        cur_boundary = cur_stdin_buf[-stop_len:]
        cur_boundary_len = len(cur_boundary)
        boundary_to_search = cur_boundary + more_buf[:stop_len-1]
        bts_ind = boundary_to_search.find(stop)
        if bts_ind >= 0:
            extra_bytes = stop_len - (cur_boundary_len - bts_ind)
            ret = get_ret([cur_stdin_buf, more_buf[:extra_bytes]])
            cur_stdin_buf = more_buf[extra_bytes:]
            return ret
        if len(more_buf) < stop_len:
            ret = get_ret([cur_stdin_buf, more_buf])
            cur_stdin_buf = []
            return ret
        ret_list.append(cur_stdin_buf)
        cur_stdin_buf = more_buf
        stop_ind = cur_stdin_buf.find(stop)
    num_bytes_from_cur = stop_ind + stop_len
    bytes_from_cur = cur_stdin_buf[:num_bytes_from_cur]
    cur_stdin_buf = cur_stdin_buf[num_bytes_from_cur:]
    return get_ret([bytes_from_cur])

def get_newline(s: bytes) -> bytes:
    last2 = s[-2:]
    if last2 == b'\r\n': # Windows
        return last2
    last = s[-1:]
    if last == b'\n': # Unix
        return last
    return b'' # end of input perhaps

def lex(lang: str) -> bool:
    if lang.startswith("py"):
        lexer = pygments.lexers.python.PythonLexer()
    elif lang == "cpp":
        lexer = pygments.lexers.c_cpp.CppLexer()
    else:
        print("Invalid language:", lang, file=sys.stderr)
        return False

    # loop over all the extracted functions
    while True:
        # Note that unix newline is \n and windows is \r\n,
        # so reading until \n works for both.
        header_bytes = read_stdin_until(b'\n')
        header_newline = get_newline(header_bytes)
        header_line = header_bytes.decode('utf-8')
        if not header_line:
            break
        if not header_line.startswith("<source file="):
            print(b"Invalid function designation: " + header_line, file=sys.stderr)
            return False

        # get the function body
        end_delim = b'</source>' + header_newline
        body_bytes = read_stdin_until(b'\n' + end_delim)[:-len(end_delim)]
        encoding = cchardet.detect(body_bytes)['encoding']
        body_str = body_bytes.decode(encoding)

        print(header_line, end="")  # header_line already has the appropriate newline at the end
        toks = lexer.get_tokens_unprocessed(body_str)
        running_string_lit = ""
        # print tokens, with a little bit of processing
        for (_, typ, tok) in toks:
            # conglomerate string literal tokens, to make them like the string lits output by txl
            if typ is Token.Literal.String:
                running_string_lit += tok
                continue
            if running_string_lit:
                print(running_string_lit)
                running_string_lit = ""
            # elide whitespace tokens
            if typ is Token.Text and not tok.strip():
                continue
            # elide comments
            if typ in Token.Comment:
                continue
            print(tok)
        if running_string_lit:
            print(running_string_lit)
        print("</source>")

    return True
