#!/usr/bin/env python3
# Note that despite the extension .x, this is a python3 file.
# This is a hacky way to bypass the usage of txl in favor of
# pygments, which is better supported.

from pygments_tokenize import lex

if not lex("cpp"):
  exit(1)
